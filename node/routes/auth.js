const router = require('express').Router();
const passport = require('passport');

const CLIENT_URL = 'http://localhost:3000';

router.get("/login/failed", (req, res) => {
    res.status(401).json({
        succes:false,
        message: "Login failed"
    })
})
router.get("/logout", (req, res) => {
    req.logout()    
    res.redirect(CLIENT_URL)
})

router.get("/login/success", (req, res) => {
    if(req.user){
        res.status(200).json({
            succes:true,
            message: "Login success",
            user: req.user,
            // cookies: req.cookies
        })
    }
    res.status(401).json({
        succes:false,
        message: "Login failed"
    })
})

router.get('/google', passport.authenticate('google', {
    scope: ['profile']
}))

router.get("/google/callback" , passport.authenticate('google', {
    successRedirect: CLIENT_URL,
    failureRedirect: '/login/failed'
    
}))

module.export = router;