const cookieSession = require('cookie-session');
const express = require('express');
const cors = require('cors');
const passport = require('passport');
const app = express();
const authRoute = require('./routes/auth');
const passportSetup = require('./passport');
app.use(cookieSession({
    name: 'session',  
    keys: ['movie'],
    maxAge: 24 * 60 * 60 * 100
}))
app.use(passport.initialize());
app.use(passport.session());

app.use(cors({
    origin: 'http://localhost:3000',
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    Credentials: true,
}))
app.use("/auth", authRoute);
app.listen(5000, () => {
    console.log('Server is running on port 50saddas00');
})