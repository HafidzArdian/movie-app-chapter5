import "./Header.scss";

import { Link, useLocation } from "react-router-dom";
import Modal, { ModalContent } from "../modal/Modal";
import React, { useEffect, useRef, useState } from "react";

import Button from "../button/Button";
import Login from "../layout/login/Login";
import { OutlineButton } from "../button/Button";
import Register from "../layout/register/Register";
import banteng from "../../assets/banteng.png";

const headerNav = [
  {
    display: "Home",
    path: "/",
  },

  {
    display: "Movies",
    path: "/movie",
  },
  {
    display: "TV Series",
    path: "/tv",
  },
  {
    display: "Profile",
    path: "/Profile",
  },
];

const Header = () => {

  const location = useLocation();
  const isHomeRoute =
    location.pathname === "/Home" || location.pathname === "/home";
  const { pathname } = useLocation();
  const headerRef = useRef(null);
  const [showModal, setShowModal] = useState(false);
  const [activeTab, setActiveTab] = useState("login");
  const [showLoginModal, setShowLoginModal] = useState(false);
const [showRegisterModal, setShowRegisterModal] = useState(false);

  const active = headerNav.findIndex((e) => e.path === pathname);

  const openModal = (tab) => {
    if (tab === "login") {
      setShowLoginModal(true);
    } else if (tab === "register") {
      setShowRegisterModal(true);
    }
  };
  
  const closeModal = () => {
    setShowModal(false);
  };

  useEffect(() => {
    const shrinkHeader = () => {
      if (
        document.body.scrollTop > 100 ||
        document.documentElement.scrollTop > 100
      ) {
        headerRef.current.classList.add("shrink");
      } else {
        headerRef.current.classList.remove("shrink");
      }
    };
    window.addEventListener("scroll", shrinkHeader);
    return () => {
      window.removeEventListener("scroll", shrinkHeader);
    };
  }, []);

  return (
    <div ref={headerRef} className="header">
    <div className="header__wrap container">
      <div className="logo">
        <img src={banteng} alt="" id="logo-banteng" />
        <Link to="/">Movie List</Link>
      </div>
      <ul className="header__nav">
        {headerNav.map((e, i) => (
          <li key={i} className={`${i === active ? "active" : ""}`}>
            <Link to={e.path}>{e.display}</Link>
          </li>
        ))}
      </ul>
      {!isHomeRoute && (
        <div className="btns">
          <Button onClick={() => openModal("login")} id="login-btn">
            Login
          </Button>
          <OutlineButton onClick={() => openModal("register")}>
            Register
          </OutlineButton>
        </div>
      )}
    </div>
    <Modal
      active={showLoginModal}
      id="login-modal"
      className="header-modal"
      onClose={() => setShowLoginModal(false)}
    >
      <ModalContent className="header-modal__content">
        <Login />
        <p>
          Don't have an account yet?{" "}
          <a href="#" onClick={() => {
              setShowLoginModal(false);
              setShowRegisterModal(true);
            }}>
            Register
          </a>
        </p>
      </ModalContent>
    </Modal>
    <Modal
      active={showRegisterModal}
      id="register-modal"
      className="header-modal"
      onClose={() => setShowRegisterModal(false)}
    >
      <ModalContent className="header-modal__content">
        <Register />
        <p>
          Already have an account?{" "}
          <a href="#" onClick={() => {
              setShowRegisterModal(false);
              setShowLoginModal(true);
            }}>
            Login
          </a>
        </p>
      </ModalContent>
    </Modal>
  </div>
  );
};

export default Header;
