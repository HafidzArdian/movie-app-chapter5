import Button, { OutlineButton } from "../../button/Button";

const Login = () => {
  const google = () => {
    window.open("http://localhost:5000/auth/google", "_self");
  }
  return (
    <form autoComplete="off">
      <div>
        <label htmlFor="email">Email Address</label>
        <br />
        <input id="email-login" type="email" placeholder="Email Address" />
      </div>
      <div>
        <label htmlFor="password">Password</label>
        <br />
        <input id="password-login" type="password" placeholder="Password" />
      </div>
       <br />
      <div>
        <Button type="button" className="small" link="/home">
          Login
        </Button>
        <OutlineButton
          className="small"
          type="button"
          onClick={google}
        >
          Using Google
        </OutlineButton>
        <br />
      </div>
    </form>
  );
};

export default Login;
