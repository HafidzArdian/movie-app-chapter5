import Button, { OutlineButton } from "../../button/Button";
import { GoogleAuthProvider, getAuth, signInWithPopup } from "firebase/auth";

// import { useState } from "react";

const Register = () => {
  const handleGoogleRegister = () => {
    const auth = getAuth();
    const provider = new GoogleAuthProvider();

    // show Google login popup
    signInWithPopup(auth, provider)
      .then((result) => {
        console.info(result.user);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  return (
    <div>
      <form>
        <div className="mb-4">
          <label htmlFor="firstName">First Name</label>
          <br />
          <input id="firstName" type="text" placeholder="First Name" />
        </div>
        <div className="mb-4">
          <label htmlFor="lastName">Last Name</label>
          <br />
          <input id="lastName" type="text" placeholder="Last Name" />
        </div>
        <div className="mb-4">
          <label htmlFor="email">Email Address</label>
          <br />
          <input id="email" type="email" placeholder="Email Address" />
        </div>
        <div className="mb-4">
          <label htmlFor="password">Password</label>
          <br />
          <input id="password" type="password" placeholder="Password" />
        </div>
        <div className="mb-6">
          <label htmlFor="confirmPassword">Confirm Password</label>
          <br />
          <input
            id="confirmPassword"
            type="password"
            placeholder="Confirm Password"
          />
        </div>
        <br />
        <div>
          <Button className="small" type="submit">
            Register
          </Button>
          <br />
          <OutlineButton
            className="small"
            type="button"
            onClick={handleGoogleRegister}
          >
            Using Google
          </OutlineButton>
        </div>
      </form>
    </div>
  );
};

export default Register;
