import Button, { OutlineButton } from "../../button/Button";

import { Link } from "react-router-dom";
import React from "react";

const Profile = () => {
  const logout = () => {
    window.open("http://localhost:5000/auth/logout", "_self");
  }
  return (
    <main>
      <h1 >Welcome</h1>
      <img src=" https://images.pexels.com/photos/1065084/pexels-photo-1065084.jpeg?auto=compress&cs=tinysrgb&w=1600" alt="" />
      <h3>MovieList.com</h3>
      <Link to="/home">
        <Button type="button" className="small">
          Home
        </Button>
      </Link>
      <Link to="/">
        <OutlineButton type="button" className="small" onClick={logout}>
          Log Out
        </OutlineButton>
      </Link>
    </main>
  );
};

export default Profile;
