import { getAuth } from "firebase/auth";
import { initializeApp } from "firebase/app";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCE8CG985G9GeNi1LRlbEG8rfGEEqveS5A",
  authDomain: "movie-app-e7d2c.firebaseapp.com",
  projectId: "movie-app-e7d2c",
  storageBucket: "movie-app-e7d2c.appspot.com",
  messagingSenderId: "153661054158",
  appId: "1:153661054158:web:99765266357f73c419449f"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
