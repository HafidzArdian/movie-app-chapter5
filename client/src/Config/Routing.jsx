import { Route, Routes } from "react-router-dom";

import Catalog from "../pages/Catalog";
import Detail from "../pages/Detail";
import Home from "../pages/Home";
import React from "react";

const Routing = () => {
  return (
    <Routes>
      <Route path="/:category/search/:keyword" component={Catalog} />
      <Route path="/:category/:id" component={Detail} />
      <Route path="/:category" component={Catalog} />
      <Route path="/:category" component={Catalog} />
      <Route path="/" exact component={Home} />
    </Routes>
  );
};

export default Routing;
