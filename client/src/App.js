import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";
import "./App.scss";

import {
  BrowserRouter,
  Outlet,
  Route,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import React, { useEffect, useState } from "react";

import Catalog from "./pages/Catalog";
import Detail from "./pages/detail/Detail";
import ErorPage from "./components/ErorrPage/404";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Home from "./pages/Home";
import Profile from "./components/layout/profile/Profile";
import { auth } from "./firebase";

// import Routing from "./Config/Routing";

function App() {
  const [user, setUser] = useState(null);
  useEffect(() => {
    const getUser = async () => {
      fetch("http://localhost:5000/auth/login/succes", {
        method: "GET",
        credentials: "include",
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Credentials": "true",
      },
      }).then(response => {
        if(response.status === 200){
          return response.json();
          throw new Error("Authentication failed")
        }
      } ).then(resObject=>{
        setUser(resObject.user);
      }).catch(err=>{
        console.log(err)
      });
    } 
    getUser();
  })
  console.log(user)
  const Layout = () => {
    return (
      <>
        <Header />
        <Outlet />
        <Footer />
      </>
    );
  };

  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout />,
      errorElement: <ErorPage />,
      children: [
        {
          path: "/",
          element: <Home />,
        },
        {
          path: "/:category",
          element: <Catalog />,
        },
        {
          path: "/:category/search/:keyword",
          element: <Catalog />,
        },
        {
          path: "/:category/:id",
          element: <Detail />,
        },
      ],
    },
    {
      path: "/profile",
      element: <Profile />,
      errorElement: <ErorPage />,
    },
  ]);
  return (
    <React.StrictMode>
      <div>
        <RouterProvider router={router} />
      </div>
    </React.StrictMode>
    // <BrowserRouter>
    //   <Routes>
    //     <Route
    //       path="/"
    //       animate={true}
    //       element={
    //         <React.Fragment>
    //           <Header />

    //           <Footer />
    //           {/* <Routing /> */}
    //         </React.Fragment>
    //       }
    //     />

    //     <Route
    //       path="/Movie"
    //       element={
    //         <React.Fragment>
    //           <Catalog />
    //         </React.Fragment>
    //       }
    //     />
    //   </Routes>
    // </BrowserRouter>

    // <BrowserRouter>
    // <Header />
    // <Routes>
    //   <Route path="/" element={<Home />}/>
    //   <Route path="/Catalog" element={<Catalog />}/>
    //   <Route path="/Detail" element={<Detail />}/>
    // </Routes>
    // <Footer />

    // </BrowserRouter>
  );
}

export default App;
